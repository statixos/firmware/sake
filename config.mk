AB_OTA_PARTITIONS += \
    aop \
    bluetooth \
    cpucp \
    devcfg \
    dsp \
    featenabler \
    hyp \
    keymaster \
    modem \
    multiimgoem \
    qupfw \
    qweslicstore \
    shrm \
    tz \
    uefisecapp \
    xbl \
    xbl_config
