LOCAL_PATH := $(call my-dir)

ifeq ($(TARGET_DEVICE),sake)

FIRMWARE_IMAGES := $(wildcard $(LOCAL_PATH)/images/*)

$(foreach f, $(notdir $(FIRMWARE_IMAGES)), \
  $(call add-radio-file,images/$(f)))

include vendor/firmware/sake/config.mk

endif
